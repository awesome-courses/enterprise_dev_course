package com.stringconcat.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EnterpriseDemoApplication

fun main(args: Array<String>) {
	runApplication<EnterpriseDemoApplication>(*args)
}
